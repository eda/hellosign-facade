var unirest = require('unirest');
var baseUri = "https://api.hellosign.com/v3";
var auth;
var hellosign;

var initialize = function initialize(hellosignCredentials){
  var HELLOSIGN_KEY = hellosignCredentials.apiKey;
  auth = {
      user:HELLOSIGN_KEY,
      pass:"",
      sendImmediately:true
    };
  hellosign = require('hellosign-sdk')({key:HELLOSIGN_KEY});
  return {
    getTemplateList:getTemplateList,
    signTemplate: signTemplate
  };
};

var headers ={
    'Accept':'application/json',
    'Content-Type':'application/json'
  };

var getTemplateList = function getTemplateList(callback){
  unirest.get(baseUri + '/template/list')
    .auth(auth)
    .header(headers)
    .end(function(response){
      callback(null,response);
    });
};

var signTemplate = function signTemplate(student, callback){
  var tcOptions = {
    test_mode:1,
    template_id: 'aa67d4e8143421720fba63b326656e63aff924eb',
    subject: 'Dev Academy Terms and Conditions',
    message: 'Hi ' + student.firstName + 'Please sign where required',
    signers:[
      {
        email_address: student.email,
        name: student.firstName,
        role: 'Student'
      }
    ],
    custom_fields: {
      "Student Name": student.firstName
    }
  };

    hellosign.signatureRequest.sendWithTemplate(tcOptions)
      .then(function(response){
        callback(response);
      })
      .catch(function(err){
        callback(err);
      });
  // }
};
module.exports = initialize;
